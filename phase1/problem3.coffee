$.getJSON 'problem3.json', (data) ->
  plot3 data.categories, data.series
  

plot3 = (categories, series) ->

  Highcharts.chart 'problem3', 
    chart: 
      type: 'bar'
    title: 
      text: 'Extra runs conceded (by team) for the year 2016'

    xAxis: 
      categories: categories
      title: 
        text: null
  
    yAxis: 
      min: 0,
      title: 
        text: 'Matches',
        align: 'high'
    labels: 
      overflow: 'justify'

    tooltip: 
      valueSuffix: ' runs'

    plotOptions: 
      bar: 
        dataLabels: 
          enabled: true
      
    credits: 
      enabled: false

    series: [{
      name: 'Extra runs conceded',
      data: series
    }]
