$.getJSON 'problem2.json', (data) ->
  plot2 data.categories, data.series
  

plot2 = (categories, series) ->

  Highcharts.chart 'problem2', 
    chart: 
      type: 'column'
    title: 
      text: 'IPL matches won (by season)'

    xAxis: 
      categories: categories
      title: 
        text: null
  
    yAxis: 
      min: 0,
      title: 
        text: 'Matches',
        align: 'high'
    labels: 
      overflow: 'justify'

    tooltip: 
      valueSuffix: ' matches'

    plotOptions: 
      column:
        stacking: 'normal' 
        dataLabels: 
          enabled: true
      
    credits: 
      enabled: false

    series: for year, data of series
      name: year, data: data
