$.getJSON 'problem1.json', (data) ->
  plot data.categories, data.data
  

plot = (categories, series) ->

  Highcharts.chart 'problem1', 
    chart: 
      type: 'bar'
    title: 
      text: 'IPL matches played per Season'

    xAxis: 
      categories: categories
      title: 
        text: null
  
    yAxis: 
      min: 0,
      title: 
        text: 'Matches',
        align: 'high'
    labels: 
      overflow: 'justify'

    tooltip: 
      valueSuffix: ' matches'

    plotOptions: 
      bar: 
        dataLabels: 
          enabled: true
      
    credits: 
      enabled: false

    series: [{
      name: 'Total Matches played',
      data: series
    }]
