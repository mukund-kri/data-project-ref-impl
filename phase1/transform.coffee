import { createReadStream, writeFile } from 'fs'
import csv from 'csv-parser'


matchesPlayedPerYear = {}
matchesWon = {}
matchId2year = {}
extraRuns2016 = {}
bowlerStats = {}


accum = (acc, row) ->
  for year in [2008..2017]
    acc[year] = [] unless acc[year]
    acc[year].push (if row[year]? then row[year] else 0)
  acc

createReadStream 'matches.csv'
  .pipe csv()

  .on 'data', (data) ->
    year = data.season
    winner = data.winner

    # Problem 1
    matchesPlayedPerYear[year] = 0 unless matchesPlayedPerYear[year]?
    matchesPlayedPerYear[year] += 1

    # Problem 2
    if winner != ''
      matchesWon[winner] = {} unless matchesWon[winner]?
      matchesWon[winner][year] = 0 unless matchesWon[winner][year]
      matchesWon[winner][year] += 1

    # problem 3 -- 1st part
    matchId2year[data.id] = year
    
  .on 'end', () ->
    # dump out problem 1 data
    problem1Data = {categories: [], data: []}
    for key, value of matchesPlayedPerYear
      problem1Data.categories.push key
      problem1Data.data.push value
    writeFile './public/problem1.json', (JSON.stringify problem1Data), (err) ->
      console.log err

    # dump out problem 2 data
    a = (value for key, value of matchesWon)
      .reduce accum, {}
    # console.log matchesWon
    problem2Data =
      categories: Object.keys matchesWon
      series: a
    writeFile './public/problem2.json', (JSON.stringify problem2Data, null, 2), (err) ->
      console.log err
      
    
    processDeliveries()


processDeliveries = () ->
  createReadStream 'deliveries.csv'
  .pipe csv()

  .on 'data', (data) ->
    year = matchId2year[data.match_id]
    team = data.bowling_team
    bowler = data.bowler

    # Problem 3
    if year == '2016'
      extraRuns2016[team] = 0 unless extraRuns2016[team]
      extraRuns2016[team] += Number(data.extra_runs)

    # Problem 4
    if year == '2015'
      bowlerStats[bowler] = {runs: 0, balls: 0} unless bowlerStats[bowler]?
      bowlerStats[bowler].runs += Number(data.total_runs)
      bowlerStats[bowler].balls += 1
      
  .on 'end', () ->
    problem3Data =
      categories: Object.keys extraRuns2016
      series: (v for k, v of extraRuns2016)
    writeFile './public/problem3.json', (JSON.stringify problem3Data, null, 2), (err) ->
      console.log err

    arrOfObjs = for k, v of bowlerStats
        name: k, economy: (v.runs * 6) / v.balls, balls: v.balls

    sorted = arrOfObjs.sort (a, b) -> a.economy - b.economy
      .filter (x) -> x.balls > 36
    sorted = sorted[0..10]
      
    problem4Data =
      categories: sorted.map (b) -> b.name
      series: sorted.map (b) -> b.economy

    writeFile './public/problem4.json', (JSON.stringify problem4Data, null, 2), (err) ->
      console.log err
