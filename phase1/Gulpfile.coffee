gulp = require 'gulp'
coffee = require 'gulp-coffee'


gulp.task 'watch', () ->
  gulp.watch 'problem*.coffee', ['compile']

gulp.task 'compile', () ->
  gulp.src 'problem*.coffee'
    .pipe (coffee bare: true)
    .pipe (gulp.dest './public/')
    
gulp.task 'default', ['compile', 'watch']
