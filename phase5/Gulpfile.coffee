gulp          = require 'gulp'
spawn         = require('child_process').spawn
webpack       = require 'webpack-stream'

webpackConfig = require './webpack.config.coffee'


gulp.task 'frontend', ->
  gulp.src './frontend/main.js'
    .pipe (webpack webpackConfig)
    .pipe (gulp.dest 'dist/')

gulp.task 'server', ->
  spawn 'nodemon', ['--watch', './server', '--exec', 'coffee', '-t', 'server/app.coffee'], stdio: 'inherit'


gulp.task 'default', ['frontend', 'server'] 
