import express  from 'express'
import mongoose from 'mongoose'

import routes   from './routes.coffee'


CONFIG =
  mongoUrl: 'mongodb://127.0.0.1/dataexercise'

# Use node's promise lib in moongoose
mongoose.Promise = global.Promise

# Extablish connection to mongoose
mongoose.connect CONFIG.mongoUrl,
  useMongoClient: true

# Initialize the main express instance
app = express()

# Express configuration
app.set 'view engine', 'pug'
app.set 'views', './server/views'

app.use (express.static 'public')
app.use '/frontend', (express.static 'dist')

app.use '/', routes


# Run the server in an infinite loop
app.listen 3000, () ->
  console.log 'running server on port 3000'

