# Full stack data exercise v2

## The stack

1. Mongo + mongoose.js
2. Express. For REST API.
3. React and Redux
4. Webpack. For building the frontend.
5. Gulp. For utility tasks and backend.
