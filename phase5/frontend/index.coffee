import React    from 'react'
import ReactDOM from 'react-dom'
import { Provider } from 'react-redux'
import { createStore } from 'redux'

import Main     from './container/MatchSchedule'
import SampleReducer from './reducers/sample'

store = createStore SampleReducer


ReactDOM.render(
  <Provider store={store} >
    <Main />
  </Provider>,
  document.getElementById 'container'
)
