import React from 'react'
import ReactDOM from 'react-dom'


export default ({toggleState, name}) ->
  <h1 onClick={ toggleState }>{ name }</h1>
