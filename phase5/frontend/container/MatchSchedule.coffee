import { connect } from 'react-redux'

import MatchScheduleComponent from '../component/MatchSchedule'


mapStateToProps = (state, ownProps) ->
  name: state.name

mapDispatchToProps = (dispatch, ownProps) ->

  toggleState: () ->
    dispatch type: 'TOGGLE'



MatchScheduleContainer = connect(
  mapStateToProps,
  mapDispatchToProps
) (MatchScheduleComponent)

export default MatchScheduleContainer

