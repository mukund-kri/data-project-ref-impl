var ajaxPopulateMenu, mkMatchTable, mkTeamMenuDiv, mkYearDiv, mkYearDivTitle;

mkMatchTable = function(teamName, year) {
  return $.getJSON("/schedule/" + year + "/" + teamName, function(data) {
    var i, len, match, results, table;
    table = $('<table>', {
      'class': 'match-data'
    });
    results = [];
    for (i = 0, len = data.length; i < len; i++) {
      match = data[i];
      table.append("<tr>\n  <td>" + match.date + "</td>\n  <td>" + match.venue + "</td>\n  <td>" + match.team1 + "</td>\n  <td>" + match.team2 + "</td>  \n</tr>");
      results.push(($("#content")).html(table));
    }
    return results;
  });
};

mkTeamMenuDiv = function(teamName, year) {
  var ele;
  ele = $("<div class='menu-team'>\n  <span class='spacer'></span>\n  <span>" + teamName + "</span>\n</div>");
  return ele.click(function() {
    return mkMatchTable(teamName, year);
  });
};

ajaxPopulateMenu = function(parent) {
  var year;
  parent.data().filled = true;
  year = parent.data('year');
  return $.getJSON("/teams/" + year, function(data) {
    return data.map(function(team) {
      return mkTeamMenuDiv(team, year);
    }).forEach(function($team) {
      return parent.append($team);
    });
  });
};

mkYearDivTitle = function(year, div) {
  var ele;
  ele = $("<div>\n  <span class='expander'></span>\n  <span>" + year + "</span>\n</div>");
  ele.click(function() {
    var i, itm, len, ref;
    ref = $('.active');
    for (i = 0, len = ref.length; i < len; i++) {
      itm = ref[i];
      if (!($(itm)).is(div)) {
        ($(itm)).toggleClass("collapsed active");
      }
    }
    if (!(div.data('filled'))) {
      ajaxPopulateMenu(div);
    }
    return div.toggleClass("collapsed active");
  });
  return ele;
};

mkYearDiv = function(year) {
  var div;
  div = $('<div>', {
    'class': 'years collapsed'
  });
  div.data({
    filled: false,
    year: year
  });
  div.append(mkYearDivTitle(year, div));
  return div;
};

$.getJSON('/years', function(data) {
  var menu;
  menu = $('#menu');
  return data.map(function(year) {
    return mkYearDiv(year);
  }).forEach(function($year) {
    return menu.append($year);
  });
});
