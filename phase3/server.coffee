import express from 'express'
import mongoose from 'mongoose'

import routes from './server/routes.coffee'

CONFIG =
  mongoUrl: 'mongodb://127.0.0.1/dataexercise'


mongoose.connect CONFIG.mongoUrl,
  useMongoClient: true
  
mongoose.Promise = global.Promise;


# Initialize an express app (One for the whole project)
app = express()

# Express configuration
app.set 'view engine', 'pug'
app.use (express.static 'public')

app.use '/', routes

# Run the server in an infinite loop
app.listen 3000, () ->
  console.log 'running server on port 3000'

  

