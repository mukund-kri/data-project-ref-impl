# A more functional version of the jQuery exercise

mkMatchTable = (matches) ->
  table = $('<table>', {'class': 'match-data'})
  for matchId, match of matches
    table.append """
<tr>
  <td>#{match.date}</td>
  <td>#{match.venue}</td>
  <td>#{match.team1}</td>
  <td>#{match.team2}</td>  
</tr>"""
  table


mkYearDivTitle = (year, div) ->
  ele = $ """
  <div>
    <span class='expander'></span>
    <span>#{year}</span>
  </div>
  """
  
  ele.click () ->
    # the for loop is to close all other active menu items
    for itm in ($ '.active')
      if  !($ itm).is(div)
        ($ itm).toggleClass "collapsed active"
    div.toggleClass "collapsed active"
  ele

 
mkTeamMenuDiv = (teamName, matches) ->
  ele = $ """
<div class='menu-team'>
  <span class='spacer'></span>
  <span>#{teamName}</span>
</div>
"""
  ele.click () ->
    ($ '#content').html (mkMatchTable matches)


mkYearDiv = (year, team) ->
  div = $ '<div>', 'class': 'years collapsed'
  div.append mkYearDivTitle year, div

  ([k, v] for k, v of team)
  .map ([teamName, matches]) -> mkTeamMenuDiv teamName, matches
  .forEach (k) -> div.append k
  div
  

$.getJSON 'data.json', (data) ->
  menu = ($ '#menu')
  
  ([year, teams] for year, teams of data)
  .map ([year, team]) -> mkYearDiv year, team
  .forEach ($year) -> menu.append $year

  
