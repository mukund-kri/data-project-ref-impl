var mkMatchTable, mkMatchesDropdown, mkYearDiv, to_slug;

to_slug = function(str) {
  str = str.replace(/\s+/g, "-").toLowerCase();
  return str;
};

mkMatchTable = function(matches) {
  var match, matchId, table;
  table = $('<table>', {
    'class': 'match-data'
  });
  for (matchId in matches) {
    match = matches[matchId];
    table.append("<tr>\n  <td>" + match.date + "</td>\n  <td>" + match.venue + "</td>\n  <td>" + match.team1 + "</td>\n  <td>" + match.team2 + "</td>  \n</tr>");
  }
  return table;
};

mkMatchesDropdown = function(teams, year) {
  var div, matchSpan, matches, results, sTeamName, teamName;
  results = [];
  for (teamName in teams) {
    matches = teams[teamName];
    div = $('<div>', {
      'class': "match-ids match-" + year
    });
    div.append($('<span>', {
      'class': 'spacer'
    }));
    sTeamName = to_slug(teamName);
    matchSpan = $('<span>', {
      id: "matchId-" + sTeamName + "-menu"
    }).text(teamName);
    div.append(matchSpan);
    matchSpan.click(function(element) {
      var team;
      team = $(element.target).text();
      matches = teams[team];
      return $('#content').html(mkMatchTable(matches));
    });
    results.push(div);
  }
  return results;
};

mkYearDiv = function(year, teams) {
  var div, expander, yearTopDiv;
  div = $('<div>');
  yearTopDiv = $('<div>', {
    id: year + "-top"
  });
  expander = ($('<span>', {
    'class': 'expander'
  })).text('+');
  yearTopDiv.append(expander);
  yearTopDiv.append(($('<span>')).text(year));
  div.append(yearTopDiv);
  div.append(mkMatchesDropdown(teams, year));
  yearTopDiv.click(function() {
    if (expander.text() === '+') {
      expander.text('-');
      return $(".match-" + year).toggle();
    } else {
      expander.text('+');
      return $(".match-" + year).toggle();
    }
  });
  return div;
};

$.getJSON('data.json', function(data) {
  var menu, results, year;
  menu = $('#menu');
  results = [];
  for (year in data) {
    results.push(menu.append(mkYearDiv(year, data[year])));
  }
  return results;
});
