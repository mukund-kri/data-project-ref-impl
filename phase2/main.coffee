to_slug = (str) ->
  str = str.replace(/\s+/g, "-").toLowerCase() # trim and force lowercase
  str


mkMatchTable = (matches) ->
  table = $('<table>', {'class': 'match-data'})
  for matchId, match of matches
    table.append """
<tr>
  <td>#{match.date}</td>
  <td>#{match.venue}</td>
  <td>#{match.team1}</td>
  <td>#{match.team2}</td>  
</tr>"""
  table

mkMatchesDropdown = (teams, year) ->

  for teamName, matches of teams
    div = $('<div>', {'class': "match-ids match-#{year}"})
    div.append $('<span>', {'class': 'spacer'})
    sTeamName = to_slug teamName
    matchSpan = $('<span>', {id: "matchId-#{sTeamName}-menu"}).text teamName
    div.append matchSpan
  
    matchSpan.click (element) ->
      team = $(element.target).text()
      matches = teams[team]
      $('#content').html (mkMatchTable matches)

    div

mkYearDiv = (year, teams) ->
  div = $('<div>')
  yearTopDiv = $('<div>', {id: "#{year}-top"})
  
  expander = ($ '<span>', {'class': 'expander'}).text '+'
  yearTopDiv.append expander
  yearTopDiv.append ($ '<span>').text year

  div.append yearTopDiv
  div.append mkMatchesDropdown(teams, year)
  
  yearTopDiv.click () ->
    if expander.text() == '+'
      expander.text '-'
      $(".match-#{year}").toggle()
    else
      expander.text '+'
      $(".match-#{year}").toggle()
  div



$.getJSON 'data.json', (data) ->

  menu = ($ '#menu')

  for year of data
    menu.append mkYearDiv(year, data[year])

  
