import React from 'react'

import Year from './Year'


export default ({years, activeYear, setActiveYear, showSchedule}) ->
  yearEles = for year, teams of years
    <Year
      key = { year }
      yearName = { year }
      teams = { teams }
      activeYear = { year==activeYear }
      setActiveYear = { setActiveYear }
      showSchedule = { showSchedule } />
  <div>
    {yearEles}
  </div>
