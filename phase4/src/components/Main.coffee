import React from 'react'

import Menu from './Menu'
import Schedule from './Schedule'


export default class Main extends React.Component  

  constructor: (props) ->
    super(props)
    @state = data: {}, activeYear: '2010'
    
  componentDidMount: () =>
    fetch '/data.json'
    .then (response) =>
      return response.json()
    .then (data) =>
      @setState data: data

  setActiveYear: (year) =>
    @setState activeYear: year

  showSchedule: (year, teamName) =>
    @setState schedule: @state.data[year][teamName]
    
  render: () =>
    <div id='container'>
      <div id='menu'>
        <Menu
          years={ @state.data }
          activeYear={ @state.activeYear }
          setActiveYear={ @setActiveYear }
          showSchedule={ @showSchedule }/>
      </div>
      <div id='content'>
        <Schedule data={ @state.schedule } />
      </div>
    </div>

