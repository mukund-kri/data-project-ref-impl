import React from 'react'

export default ({data}) ->
  <div>
    <table>
      <tbody>
        {for matchId, match of data
          <tr key={ matchId } >
            <td>{ match.date }</td>
            <td>{ match.venue }</td>
            <td>{ match.team1 }</td>
            <td>{ match.team2 }</td>  
          </tr>
        }
      </tbody>
    </table>
  </div>

    
