import React from 'react'

import Team from './Team'


export default ({yearName, teams, activeYear, setActiveYear, showSchedule}) ->

  teamEles = for name of teams
    <Team teamName={name} year={yearName} showSchedule={showSchedule} key={name}/>

  cls = if activeYear then 'active' else 'collapsed'

  _setActiveYear = () =>
    if activeYear
      setActiveYear ""
    else
      setActiveYear yearName
    
  <div className={cls + ' years'}>
    <div  onClick={_setActiveYear}>
      <span className='expander'></span>
      <span>{yearName}</span>
    </div>
    { teamEles }
  </div>
