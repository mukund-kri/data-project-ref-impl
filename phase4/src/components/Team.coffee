import React from 'react'


export default ({teamName, year, showSchedule}) ->

  _showSchedule = () =>
    showSchedule(year, teamName)
    
  <div className='menu-team' onClick={ _showSchedule }>
    <span className='spacer'></span>
    <span>{teamName}</span>
  </div>
